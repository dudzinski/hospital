package hospital;

import hospital.pojo.Patient;
import hospital.pojo.Recognition;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

public class HospitalQueueService implements Comparator<Patient> {


    Queue<Patient> patiensQueue = new PriorityQueue<>(this::compare);

    public String addPatient(Patient patient) {

        patiensQueue.add(patient);
        Patient patientAdded = patiensQueue.peek();
        return patientAdded.getName() + " " + patientAdded.getSurname();
    }

    public String nextPatient() {

        if (patiensQueue.size() > 0) {
            Patient patientNext = patiensQueue.poll();
            return patientNext.getName() + " " + patientNext.getSurname() + "     -     " + patientNext.isFavoritism();
        } else {
            System.out.println("\n wszyscy obsłużeni");
            System.exit(0);
        }
        return "koniec";
    }

    @Override
    public int compare(Patient o1, Patient o2) {

        if (o1.isFavoritism()) {
            if (o1.getRecognition() == Recognition.SERIOUS_CONDITION.getInfectivity()) {
                if ((o1.getRestlessness() * o1.getRecognition()) > (o2.getRestlessness() * o2.getRecognition())) {
                    return 1;
                } else if ((o1.getRestlessness() * o1.getRecognition()) < (o2.getRestlessness() * o2.getRecognition())) {
                    return -1;
                } else {
                    return 0;
                }
            }
            return 1;
        }
        return 1;
    }
}


//    @Override
//    public int compare(Patient o1, Patient o2) {
//       if (o1.isFavoritism()) {
//            return 1;
//        } else if (o1.getRecognition() == Recognition.SERIOUS_CONDITION.getInfectivity()) {
//            return 0;
//        } else if ((o1.getRestlessness() * o1.getRecognition()) > (o2.getRestlessness() * o2.getRecognition())) {
//            return 0;
//        }
//        return -1;


