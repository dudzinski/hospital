package hospital.threads;

import hospital.HospitalQueueService;
import hospital.RandomPatientGenerator;

public class AddPatientThread extends Thread {

    public AddPatientThread(HospitalQueueService queue) {
        this.queue = queue;
    }

    RandomPatientGenerator patient = new RandomPatientGenerator();
    HospitalQueueService queue;


    @Override
    public void run() {
        while (true) {
            try {
                this.sleep(2000);
            } catch (InterruptedException exc) {
                System.out.println("Wątek zoostał przerwany w klasie Add.");
                return;
            }
            System.out.println("  -  Add Patient Thread  -");
            System.out.println(queue.addPatient(patient.randomPatient()));
        }
    }
}
