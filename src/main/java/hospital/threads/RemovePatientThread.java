package hospital.threads;

import hospital.DisplayGenerator;
import hospital.HospitalQueueService;

import java.util.Random;

public class RemovePatientThread extends Thread {

    public RemovePatientThread(HospitalQueueService queue) {
        this.queue = queue;
    }

    HospitalQueueService queue;

    @Override
    public void run() {
        while (true) {
            try {
                this.sleep(2010 + new Random().nextInt(100));
            } catch (InterruptedException exc) {
                System.out.println("Wątek został przerwany w klasie Remove.");
                return;
            }
            System.out.println("  -  Remove Patient Thread  -");
            System.out.println(queue.nextPatient());
        }
    }
}

