package hospital;

import hospital.pojo.Patient;
import hospital.pojo.Recognition;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class RandomPatientGenerator {

    public List<String> readNamesFromFile() {

        List<List<String>> namesFromFile = new ArrayList<>();
        Path namesFilePath = Paths.get("names.txt");
        try {
            namesFromFile = Files.readAllLines(namesFilePath).stream()
                    .map(x -> x.split(";"))
                    .map(x -> Arrays.asList(x))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            System.out.println("Sprawdź swój plik 'names'!");
            System.exit(0);
        } catch (NumberFormatException n) {
            System.out.println("Niepoprawny format danych w pliku 'names'!");
            System.exit(0);
        }
        List<String> names = new ArrayList<>();
        names.addAll(namesFromFile.get(0));
        return names;
    }

    public List<String> readSurnamesFromFile() {

        List<List<String>> surnamesFromFile = new ArrayList<>();
        Path surnamesFilePath = Paths.get("surnames.txt");
        try {
            surnamesFromFile = Files.readAllLines(surnamesFilePath).stream()
                    .map(x -> x.split(";"))
                    .map(x -> Arrays.asList(x))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            System.out.println("Sprawdź swój plik 'surnames'!");
            System.exit(0);
        } catch (NumberFormatException n) {
            System.out.println("Niepoprawny format danych w pliku 'surnames'!");
            System.exit(0);
        }
        List<String> surnames = new ArrayList<>();
        surnames.addAll(surnamesFromFile.get(0));
        return surnames;
    }

    public Patient randomPatient() {

        List<String> patientNames = readNamesFromFile();
        List<String> patientSurnames = readSurnamesFromFile();
        List<Integer> recognition = new ArrayList<>();
        for (Recognition e : Recognition.values()) {
            recognition.add(e.getInfectivity());
        }

        int randomNameIndex = new Random().nextInt(patientNames.size());
        int randomSurnameIndex = new Random().nextInt(patientSurnames.size());
        int randomRestlessnes = new Random().nextInt(10);
        int randomRecognitionIndex = new Random().nextInt(4);

        String randomName = patientNames.remove(randomNameIndex);
        String randomSurname = patientSurnames.remove(randomSurnameIndex);
        int randomRecognition = recognition.get(randomRecognitionIndex);

        Patient randomPatient = new Patient(randomName, randomSurname, randomRestlessnes, randomRecognition);
        return randomPatient;
    }
}
