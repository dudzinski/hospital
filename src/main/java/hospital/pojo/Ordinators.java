package hospital.pojo;

public enum Ordinators {

    MAREK_KOWALSKI("Marek Kowalski"),
    MICHAL_KOWALSKI("Michal Kowalski"),
    TOMASZ_KOWALSKI("Tomasz Kowalski"),
    TOMASZ_BARAN("Tomasz Baran"),
    JERZY_BARAN("Jerzy Baran"),
    STANISLAW_BARAN("Stanislaw Baran"),
    MARTA_BARAN("Marta Baran"),
    STANISLAW_NOWAK("Stanislaw Nowak"),
    MARTA_KOT("Marta Kot"),
    TOMASZ_KOT("Tomasz Kot"),
    MICHAL_ZAZEN("Michal Zazen"),
    JERZY_BUTLA("Jerzy Butla");

    String ordinator;

    Ordinators(String ordinator) {
        this.ordinator = ordinator;
    }

    public String getOrdinator() {
        return ordinator;
    }
}
