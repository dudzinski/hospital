package hospital.pojo;

import hospital.DisplayGenerator;
import hospital.HospitalQueueService;
import hospital.pojo.Ordinators;

public class Patient {

    private String name;
    private String surname;
    private int restlessness;
    private int recognition;
    private boolean favoritism;

    public Patient(String name, String surname, int restlessness, int recognition) {
        this.name = name;
        this.surname = surname;
        this.restlessness = restlessness;
        this.recognition = recognition;
        this.favoritism = false;
        String patientIdentity = name + " " + surname;
        for (Ordinators ordinator : Ordinators.values()) {
            if ((patientIdentity).equals(ordinator.getOrdinator())) {
                this.favoritism = true;
            }
        }
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getRestlessness() {
        return restlessness;
    }

    public int getRecognition() {
        return recognition;
    }

    public boolean isFavoritism() {
        return favoritism;
    }
}

