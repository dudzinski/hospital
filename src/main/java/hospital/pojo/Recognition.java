package hospital.pojo;

public enum Recognition {

    FLU(1),
    COLD(2),
    DIARRHEA(3),
    SERIOUS_CONDITION(4);

    int infectivity;

    Recognition(int infectivity) {
        this.infectivity = infectivity;
    }

    public int getInfectivity() {
        return infectivity;
    }
}
