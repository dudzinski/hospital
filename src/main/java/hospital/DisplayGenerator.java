package hospital;

import hospital.pojo.Patient;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

public class DisplayGenerator {

    HospitalQueueService patientQueue;

    public DisplayGenerator (HospitalQueueService patientQueue){
        this.patientQueue = patientQueue;
    }

    public void displayQueue(){

        String queueInfo = "";
        List<Patient> queue = new ArrayList<>();
        queue.addAll(patientQueue.patiensQueue);

       for ( int i=0;i<queue.size();i++){
           queueInfo += queue.get(i).getName()+" "
                   + queue.get(i).getSurname()+" "
                   + queue.get(i).isFavoritism()
                   +"\n";
       }
       JOptionPane.showMessageDialog(null   , queueInfo);
    }
}

