package hospital;

import hospital.threads.AddPatientThread;
import hospital.threads.RemovePatientThread;

import javax.swing.*;

public class Runner {

    public static void main(String[] args) {

        RandomPatientGenerator randomPatient = new RandomPatientGenerator();
        HospitalQueueService queue = new HospitalQueueService();
        AddPatientThread addTest = new AddPatientThread(queue);
        RemovePatientThread remTest = new RemovePatientThread(queue);
        DisplayGenerator displayGenerator = new DisplayGenerator(queue);


        for (int i = 0; i < 10; i++) {
            queue.addPatient(randomPatient.randomPatient());
        }

        addTest.start();
        remTest.start();

        while (true) {
            JOptionPane.showMessageDialog(null  , "Wyświetl kolejkę.");
                displayGenerator.displayQueue();
            }
        }
    }
